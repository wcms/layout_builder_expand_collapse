TODO: Figure out theming.
TODO: Figure out when the section that is the first one in group is
removed.  Currently there is no easy way to set the next section as
the first.

How it works
============
- Additional settings are added to the section configuration form
- Settings include setting a title for the overall group, selecting an
  existing group or creating a new grouping
- End result is that it adds a class and a data selector to the layout's
  HTML. With some simple custom javascript will make the expand/collapse
  functionality work.  (this is a TODO)
- In addition to the theme/template it exposes if the group is the first
  section.  This will allow the themer to be able to put the "Tab Title"
  above the first group section. (this is again a TODO).
