<?php

namespace Drupal\layout_builder_expand_collapse\Form;

use Drupal\layout_builder\Form\RemoveSectionForm as OriginalRemoveSectionForm;

/**
 * Provides removal form with access to all sections.
 *
 * @internal
 *   Form classes are internal.
 */
class RemoveSectionForm extends OriginalRemoveSectionForm {

  /**
   * Retrieve the section storage property.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface
   *   The section storage for the current form.
   */
  public function getSectionStorage() {
    return $this->sectionStorage;
  }

  /**
   * Returns protected property.
   *
   * @return int
   *   Section order number (delta).
   */
  public function getDelta() {
    return $this->delta;
  }

}
