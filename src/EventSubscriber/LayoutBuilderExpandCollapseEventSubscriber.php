<?php

namespace Drupal\layout_builder_expand_collapse\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW layout expand collapse event subscriber.
 *
 * Adds the ability to expand and collapse any section.
 */
class LayoutBuilderExpandCollapseEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Helper function to get section message.
   *
   * This is needed to pass coding standards as you cannot pass functions or
   * variables or constants to t() function.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Translated Section Message
   */
  protected function getSectionMessage() {
    return $this->t('Expand/collapse group');
  }

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent $event
   *   The event.
   */
  public function alterLayoutBuilderSectionForm(FormIdAlterEvent $event): void {
    $msg = $this->getSectionMessage();
    $formDefaultValues = [];
    $formDefaultValues['groups'] = [];
    $form = &$event->getForm();
    $formState = $event->getFormState();
    /** @var \Drupal\layout_builder\Form\ConfigureBlockFormBase $formObject */
    $formObject = $formState->getFormObject();

    // Note: getLayout() gets Current Section, not current Layout.
    $currentSectionConfig = $formObject->getLayout()->getConfiguration();

    // Search all sections to gather a list of groups.
    $sections = $formObject->getSectionStorage()->getSections();
    foreach ($sections as $section) {
      $sectionConfig = $section->getLayout()->getConfiguration();
      if (isset($sectionConfig['expand_collapse'])) {
        if (isset($sectionConfig['expand_collapse']['group'])) {
          if (!in_array($sectionConfig['expand_collapse']['group'], $formDefaultValues['groups'])) {
            $formDefaultValues['groups'][$sectionConfig['expand_collapse']['group']] = $sectionConfig['expand_collapse']['group'];
          }
        }
      }
    }

    $formDefaultValues['setting'] = $currentSectionConfig['expand_collapse']['setting'] ?? 0;
    $formDefaultValues['new'] = $currentSectionConfig['expand_collapse']['new'] ?? 0;
    $formDefaultValues['group'] = $currentSectionConfig['expand_collapse']['group'] ?? NULL;
    $formDefaultValues['group_title'] = $currentSectionConfig['expand_collapse']['group_title'] ?? NULL;
    $formDefaultValues['heading_selector'] = $currentSectionConfig['expand_collapse']['heading_selector'] ?? 'h2';
    $this->addFormElement($form, $formDefaultValues);
    // From layout_builder_styles module:
    // Our submit handler must execute before the default one, because the
    // default handler stores the section & component data in the tempstore
    // and we need to update those objects before that happens.
    // End.
    // I think its because the setConfiguration is does not get save, but if you
    // setConfiguration before, then the default saves everything perfectly.
    array_unshift(
      $form['#submit'],
      [$this, 'layoutBuilderExpandCollapseSubmit']
    );
    array_unshift(
      $form['#validate'],
      [$this, 'layoutBuilderExpandCollapseValidate']
    );

    // Only do this if expand collapse setting is enabled.
    if (isset($currentSectionConfig['expand_collapse']['setting']) &&
      $currentSectionConfig['expand_collapse']['setting'] == 1) {
      // Trim the Expand/Collapse message from the admin label.
      $label_default = $form['layout_settings']['label']['#default_value'];
      // Need trim function as there could be an extra whitespace on the end
      // that was added if the admin label was not empty.
      $label_default = trim(explode("[$msg", $label_default)[0]);
      $form['layout_settings']['label']['#default_value'] = $label_default;
    }
  }

  /**
   * Alter remove section form.
   *
   * Add an additional submit handler.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent $event
   *   The event.
   */
  public function alterLayoutBuilderRemoveSectionForm(FormIdAlterEvent $event): void {
    $form = &$event->getForm();
    // Needs to happen after default submitFormHandler, as the groups get
    // re-align after the section has been removed.
    $form['#submit'][] = [
      $this,
      'layoutBuilderExpandCollapseRemoveSectionSubmit',
    ];
  }

  /**
   * Validate handler for handling the E/C settings of the Section.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public function layoutBuilderExpandCollapseValidate(array &$form, FormStateInterface $formState) {

    // Only validate if expand/collapse setting is turned on.
    $expandCollapseValues = $formState->getValue('expand_collapse');

    if ($expandCollapseValues['setting']) {
      if ($expandCollapseValues['group'] == 'other' && empty($expandCollapseValues['new_grouping'])) {
        $formState->setError($form['expand_collapse']['new_grouping'], $this->t('You must enter a group name, or select an existing group.'));
      }
      if (preg_match("/[^A-Za-z0-9]/", $expandCollapseValues['new_grouping']) || strlen($expandCollapseValues['new_grouping']) > 255) {
        $formState->setError($form['expand_collapse']['new_grouping'], $this->t('Group names must only use A-Z or 0-9 characters, and no more than 255 characters.'));
      }
      if ($expandCollapseValues['new_grouping'] == 'other') {
        $formState->setError($form['expand_collapse']['new_grouping'], $this->t('You may not name your group "other".'));
      }
      if ($expandCollapseValues['group'] == 'other' && strlen($expandCollapseValues['group_title']) <= 0) {
        $formState->setError($form['expand_collapse']['group_title'], $this->t('Header text is required when creating a new group.'));
      }
    }
  }

  /**
   * Submit handler for handling the E/C settings of the Section.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current form state.
   */
  public function layoutBuilderExpandCollapseSubmit(array &$form, FormStateInterface $formState) {

    $expandCollapseTurnedOff = FALSE;
    $oldExpandCollapseSettings = [];
    /** @var \Drupal\layout_builder\Form\ConfigureBlockFormBase $formObject */
    $formObject = $formState->getFormObject();

    // Get expand collapse settings from form.
    $formExpandCollapseSettings = $formState->getValue('expand_collapse');

    // Note: getLayout() gets Current Section, not current Layout.
    $currentSectionConfig = $formObject->getLayout()->getConfiguration();

    // Track if the expand/collapse was on, but now it is off.
    if (isset($currentSectionConfig['expand_collapse']['setting']) &&
      $currentSectionConfig['expand_collapse']['setting'] == 1 &&
      $formExpandCollapseSettings['setting'] == 0) {
      $expandCollapseTurnedOff = TRUE;
    }

    if ($formExpandCollapseSettings['group'] == 'other' && !empty($formExpandCollapseSettings['new_grouping'])) {
      $formExpandCollapseSettings['group'] = $formExpandCollapseSettings['new_grouping'];
    }
    unset($formExpandCollapseSettings['new_grouping']);

    $oldExpandCollapseSettings = $currentSectionConfig['expand_collapse'] ?? [];
    $currentSectionConfig['expand_collapse'] = $formExpandCollapseSettings;

    $formObject
      ->getLayout()
      ->setConfiguration($currentSectionConfig);

    // Update the label of the Section.
    $label = $formState->getValue(['layout_settings', 'label']) ?? '';
    $msg = $this->getSectionMessage();

    // Append the grouping if user has set a custom 'Admin Label'.
    // Notice we are not modifying the layout's configuration layout_settings
    // label, but the form's because the configuration gets re-written by
    // Drupal\Core\Layout\LayoutDefault::submitConfigurationForm()
    // as it is the last submitHandler.  No easy way to override.
    if ($formExpandCollapseSettings['setting']) {
      if (!empty($label)) {
        $formState->setValue([
          'layout_settings',
          'label',
        ], "$label [$msg: " . $formExpandCollapseSettings['group'] . "]");
      }
      else {
        $formState->setValue([
          'layout_settings',
          'label',
        ], "Section [$msg: " . $formExpandCollapseSettings['group'] . "]");
      }
    }

    // Mark the first on in the group if its first (even if it didn't change).
    // Also set the header size for all of the group sections to be the same.
    // Realign the old group, if expand/collapse was just turned off.
    // Do not realign if the section as just added and expand/collapse
    // was never turned on.
    if ($formExpandCollapseSettings['setting'] == 1) {
      self::alignGroupSettings(
        $formState,
        $formExpandCollapseSettings['group'],
        $formExpandCollapseSettings['heading_selector'],
      );
    }
    elseif ($expandCollapseTurnedOff && isset($oldExpandCollapseSettings['group'])) {
      self::alignGroupSettings(
        $formState,
        $oldExpandCollapseSettings['group'],
      );
    }
  }

  /**
   * Submit handler for handling the E/C settings of the Section.
   *
   * Actions are needed to be taken when the section is removed.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public function layoutBuilderExpandCollapseRemoveSectionSubmit(array &$form, FormStateInterface $formState) {
    if ($formState->getValue(['op'])->getUntranslatedString() == 'Remove') {
      /** @var \Drupal\layout_builder_expand_collapse\Form\RemoveSectionForm $formObject */
      $formObject = $formState->getFormObject();

      $sections = $formObject->getSectionStorage()->getSections();
      $delta = $formObject->getDelta();

      if (isset($sections[$delta])) {
        $questionArgs = $formObject->getQuestion()->getArguments();
        if (isset($questionArgs['@section'])) {

          // Get the string position after the Section Message.
          $afterMsgPosition = strpos($questionArgs['@section'], $this->getSectionMessage()) + strlen($this->getSectionMessage());
          // Get the group substring, then trim off the
          // ': ' at the beginning and the ']' characters at the end.
          $group = substr(substr($questionArgs['@section'], $afterMsgPosition), 2, -1);
          // If the section has expand/collapse enabled then we may need to
          // re-align which group is the first in its section.
          self::alignGroupSettings($formState, $group);
        }
      }
    }
  }

  /**
   * Helper function to add the form element to the section configuration form.
   *
   * @param array $form
   *   The form.
   * @param array $defaultValues
   *   Array of default form values.
   */
  public function addFormElement(array &$form, array $defaultValues) {

    // Add other to the groups. This allows users to add new ones.
    $defaultValues['groups']['other'] = $this->t('other');

    // Heading selector options.
    $headingsOptions = [
      'h2' => 'H2',
      'h3' => 'H3',
      'h4' => 'H4',
      'h5' => 'H5',
      'h6' => 'H6',
    ];

    $form['expand_collapse'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Expand/collapse settings'),
    ];
    $form['expand_collapse']['notice'] = [
      '#type' => 'markup',
      '#markup' => '<p>Note: this is not fully implemented and will not create sections that expand/collapse at this time.</p>',
    ];
    $form['expand_collapse']['setting'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make section expand/collapsible'),
      '#required' => FALSE,
      '#default_value' => $defaultValues['setting'],
    ];
    $form['expand_collapse']['group'] = [
      '#type' => 'radios',
      '#title' => $this->t('Grouping'),
      '#required' => FALSE,
      '#default_value' => $defaultValues['group'] ?? 'other',
      '#options' => $defaultValues['groups'],
      '#description' => $this->t('Select an existing group, or select "other" to create a new grouping. The list of groups is shown in the order they appear down the page.'),
      '#states' => [
        'visible' => [
          ':input[name="expand_collapse[setting]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    // This textfield will only be shown when the option 'Other'
    // is selected from the radios above.
    $form['expand_collapse']['new_grouping'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group name'),
      '#description' => $this->t('Use only the characters A-Z and 0-9, with a maximum of 255 characters.'),
      '#attributes' => [
        'id' => 'custom-expand-collapse-grouping',
      ],
      '#states' => [
        'visible' => [
          ':input[name="expand_collapse[setting]"]' => ['checked' => TRUE],
          ':input[name="expand_collapse[group]"]' => ['value' => 'other'],
        ],
      ],
    ];
    $form['expand_collapse']['group_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Header text'),
      '#required' => FALSE,
      '#default_value' => $defaultValues['group_title'],
      '#description' => $this->t('Enter the text to use as a heading for this group.'),
      '#states' => [
        'visible' => [
          ':input[name="expand_collapse[setting]"]' => ['checked' => TRUE],
          ':input[name="expand_collapse[group]"]' => ['value' => 'other'],
        ],
      ],
    ];
    $form['expand_collapse']['heading_selector'] = [
      '#type' => 'select',
      '#title' => $this->t('Heading level'),
      '#required' => FALSE,
      '#options' => $headingsOptions,
      '#description' => $this->t('Select an appropriate heading level for the context on the page.'),
      '#default_value' => $defaultValues['heading_selector'] ?? 'h2',
      '#states' => [
        'visible' => [
          ':input[name="expand_collapse[setting]"]' => ['checked' => TRUE],
          ':input[name="expand_collapse[group]"]' => ['value' => 'other'],
        ],
      ],
    ];
    $form['expand_collapse']['new'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Start new expand/collapse set'),
      '#description' => $this->t('Give expand/collapse groups from this point forward their own expand/collapse all buttons.'),
      '#required' => FALSE,
      '#default_value' => $defaultValues['setting'],
      '#states' => [
        'visible' => [
          ':input[name="expand_collapse[setting]"]' => ['checked' => TRUE],
          ':input[name="expand_collapse[group]"]' => ['value' => 'other'],
        ],
      ],
    ];
  }

  /**
   * Helper function to mark and realign the sections.
   *
   * This marks which is first, and sets the same header for each section that
   * is part of the same group. This is done after part of the form, so we
   * always have the formObject to use.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current form state.
   * @param string $group
   *   The group.
   * @param string $headingSelector
   *   The selected header tag/size.
   */
  protected static function alignGroupSettings(FormStateInterface $formState, string $group, string $headingSelector = 'h2') {
    // I would just used $formObject instead of $formState, but
    // \Drupal\layout_builder\Form\ConfigureBlockFormBase is currently set to
    // \Drupal\layout_builder_styles\Form\ConfigureSectionForm.
    // Same thing happens for RemoveSection Form.
    /** @var \Drupal\layout_builder\Form\ConfigureBlockFormBase $formObject */
    $formObject = $formState->getFormObject();
    $isFirst = TRUE;
    // If group is set fix only that group.
    $sections = $formObject->getSectionStorage()->getSections();
    foreach ($sections as $section) {
      $sectionLayoutSettings = $section->getLayoutSettings();
      if (isset($sectionLayoutSettings['expand_collapse'])) {
        if ($sectionLayoutSettings['expand_collapse']['setting'] == 1) {
          if (isset($sectionLayoutSettings['expand_collapse']['group'])) {
            if ($sectionLayoutSettings['expand_collapse']['group'] == $group) {
              if ($isFirst) {
                $sectionLayoutSettings['expand_collapse']['is_first'] = TRUE;
                $isFirst = FALSE;
              }
              else {
                $sectionLayoutSettings['expand_collapse']['is_first'] = FALSE;
              }
              // Set heading selector so its consistent across all tabs.
              if ($headingSelector) {
                $sectionLayoutSettings['expand_collapse']['heading_selector'] = $headingSelector;
              }
              // Save configuration, only if it changed.
              $section->setLayoutSettings($sectionLayoutSettings);
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'hook_event_dispatcher.form_layout_builder_configure_section.alter' => 'alterLayoutBuilderSectionForm',
      'hook_event_dispatcher.form_layout_builder_remove_section.alter' => 'alterLayoutBuilderRemoveSectionForm',
    ];
  }

}
